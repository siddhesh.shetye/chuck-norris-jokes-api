import React, { useState, useEffect } from 'react';
import { AppBar, Toolbar, Typography, Container, TextField, Button, Card, CardContent } from '@material-ui/core';
import ButterToast, { Cinnamon, POS_CENTER, POS_BOTTOM } from 'butter-toast';
import Spinner from './Spinner';

function App() {
  const [jokes, setJokes] = useState([]);
  const [jokesToShow, setJokesToShow] = useState([]);
  const [firstName, setFirstName] = useState('Chuck');
  const [lastName, setLastName] = useState('Norris');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    fetchAndSetJokes();
  }, []);

  const fetchAndSetJokes = () => {
    fetch(`https://api.icndb.com/jokes?firstName=${firstName}&lastName=${lastName}`)
      .then((res) => res.json())
      .then((res) => {
        setJokes(res.value);
        setJokesToShow(res.value.slice(0, 10));
        setLoading(false);
      })
      .catch((err) =>
        ButterToast.raise({
          content: <Cinnamon.Crisp title='Error' content={`${err}`} scheme={Cinnamon.Crisp.SCHEME_PURPLE} />
        })
    );
  };

  const changeName = (e) => {
    e.preventDefault();
    if (firstName === '' || lastName === '') return;
    fetchAndSetJokes();
  };

  useEffect(() => {
    const lastJokejokesToShowId = document.getElementById(`${jokesToShow.length - 1}`);
    if (lastJokejokesToShowId)
    {
      observeElement(lastJokejokesToShowId);
    }
  }, [jokesToShow]);

  const observeElement = (lastJokejokesToShowId) => {
    const observer = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting === true) {
          appendJokes();
          observer.unobserve(lastJokejokesToShowId);
        }
      },
      {
        threshold: 1,
      }
    );

    observer.observe(lastJokejokesToShowId);
  };

  const appendJokes = () => {
    setLoading(true);
    setJokesToShow(jokes.slice(0, jokesToShow.length + 10));
    setLoading(false);
  };

  return (
    <div className="App">
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" align="center" >
            Chuck Norris Jokes
          </Typography>
        </Toolbar>
      </AppBar>

      <Container className="mt-20">
        <form onSubmit={changeName} noValidate>
          <TextField
            id='firstName'
            label='First Name'
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />
          <TextField
            id='lastName'
            label='Last Name'
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          />
          <Button type='submit' variant='contained' color='primary'>
            Submit
          </Button>
        </form>
      </Container>
      <Container>
        {jokesToShow.map((joke, index) => {
          return (
            <Card key={joke.id} className="mb-20" id={`${index}`}>
              <CardContent>
                <Typography>{joke.joke}</Typography>
              </CardContent>
            </Card>
          );
        })}

        {loading && <Spinner />}
      </Container>

      <ButterToast position={{ vertical: POS_BOTTOM, horizontal: POS_CENTER }} />
    </div>
  );
}

export default App;
